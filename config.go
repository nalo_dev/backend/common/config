package config

import (
	"fmt"
	"os"

	Viper "github.com/spf13/viper"
	"gitlab.com/nalo_dev/backend/common/adapter"
)

// Config struct ..
type Config struct {
	Name          string            `mapstructure:"name"`
	Port          map[string]string `mapstructure:"port"`
	Version       string            `mapstructure:"version"`
	Debug         bool              `mapstructure:"debug"`
	MSSQL         adapter.MSSQLs
	MYSQL         adapter.MYSQLs
	Redis         adapter.Redises
	RabbitMQ      adapter.Rabbits
	API           adapter.APIs
	Other         adapter.Others
	Graylog       adapter.Graylogs
}

var config *Config

func init() {
	var folder string

	env := os.Getenv("APPLICATION_ENV")

	switch env {
	case "master", "dev", "uat":
		folder = env
	default:
		folder = "local"
	}

	path := fmt.Sprintf("config/%v", folder)

	//Get base config
	config = new(Config)
	fetchDataToConfig(path, "base", config)

	//Get all sub config
	fetchDataToConfig(path, "mssql", &(config.MSSQL))
	fetchDataToConfig(path, "rabbit", &(config.RabbitMQ))
	fetchDataToConfig(path, "other", &(config.Other))
	fetchDataToConfig(path, "api", &(config.API))
	fetchDataToConfig(path, "redis", &(config.Redis))
	fetchDataToConfig(path, "graylog", &(config.Graylog))
	fetchDataToConfig(path, "mysql", &(config.MYSQL))
}

func fetchDataToConfig(configPath, configName string, result interface{}) {
	viper := Viper.New()
	viper.AddConfigPath(configPath)
	viper.SetConfigName(configName)

	err := viper.ReadInConfig() // Find and read the config file
	if err == nil {             // Handle errors reading the config file
		err = viper.Unmarshal(result)
		if err != nil { // Handle errors reading the config file
			panic(fmt.Errorf("Fatal error config file: %s", err))
		}
	}
}

// GetConfig func
func GetConfig() *Config {
	return config
}
